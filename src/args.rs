use clap::{Parser, Subcommand};

#[derive(Parser)]
#[clap(version)]
#[clap(propagate_version = true)]
pub struct Cli {
    #[clap(subcommand)]
    pub commands: Args,
}

#[derive(Subcommand)]
pub enum Args {
    /// Replay buffer commands
    Replay {
        #[clap(long, action)]
        /// Start replay buffer
        start: bool,
        #[clap(long, action)]
        /// Save replay buffer
        save: bool,
        #[clap(long, action)]
        /// Stop replay buffer
        stop: bool,
    },
}
