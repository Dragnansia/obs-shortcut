pub mod args;
pub mod error;
pub mod settings;

use crate::args::{Args, Cli};
use clap::StructOpt;
use obws::Client;
use settings::Settings;

/// Return obs client
async fn connect_to_obs(settings: Settings) -> Result<Client, obws::Error> {
    Client::connect(&settings.host, settings.port, Some(&settings.password)).await
}

#[tokio::main]
async fn main() -> Result<(), error::Error> {
    // Connect to obs with a first connection method or default value
    let obs_client = connect_to_obs(Settings::new()?).await?;

    let args = Cli::parse();
    match args.commands {
        Args::Replay { start, save, stop } => {
            if start {
                obs_client.replay_buffer().start().await?;
            }

            if save {
                obs_client.replay_buffer().save().await?;
            }

            if stop {
                obs_client.replay_buffer().stop().await?;
            }
        }
    }

    Ok(())
}
