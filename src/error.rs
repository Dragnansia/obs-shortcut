#[derive(Debug)]
pub enum Error {
    Obs(obws::Error),
    Io(std::io::Error),
    IpAddress(local_ip_address::Error),
}

impl From<obws::Error> for Error {
    fn from(err: obws::Error) -> Self {
        Self::Obs(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<local_ip_address::Error> for Error {
    fn from(err: local_ip_address::Error) -> Self {
        Self::IpAddress(err)
    }
}
