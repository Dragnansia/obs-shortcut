use std::collections::HashMap;

use ini::ini;
use local_ip_address::local_ip;

use crate::error;

/// Settings used to connect to obs websocket
///
/// The only default value is the port '4455',
/// but you can't connect to obs with just this.
///
/// Check if is not possible to get connection data
/// from obs config or anything else and used it to connect
///
/// Settings file: ~/.config/obs-studio/global.ini
///     - Server password
///     - Server port
pub struct Settings {
    pub host: String,
    pub port: u16,
    pub password: String,
}

impl Settings {
    /// Read file config and if not found one or any neccessary information
    /// is not on this file return a Error
    pub fn new() -> Result<Self, error::Error> {
        let host = local_ip()?.to_string();
        let obs_datas = Self::get_obs_global_init();
        let obs_websocket = obs_datas.get("obswebsocket").unwrap();
        let password = obs_websocket
            .get("serverpassword")
            .unwrap()
            .clone()
            .unwrap();
        let port = obs_websocket
            .get("serverport")
            .unwrap()
            .clone()
            .unwrap()
            .parse::<u16>()
            .unwrap();

        Ok(Self {
            host,
            port,
            password,
        })
    }

    fn get_obs_global_init() -> HashMap<String, HashMap<String, Option<String>>> {
        let dir = dirs::config_dir().unwrap();

        ini!(&format!("{}/obs-studio/global.ini", dir.to_str().unwrap()))
    }
}
