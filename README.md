## Obs Shortcut
Just a small project to call obs shortcut on wayland, it's only for personnal usage but you can used it if you want.

#### Commands
- Replay buffer
```bash
obs-shortcut replay --start
obs-shortcut replay --stop
obs-shortcut replay --save
```

### Informations
For Fedora we need to install obs-websocket plugin with latest version to have the correct configuration on `global.ini`.

#### Todo
- [x] Find a way to get obs connection data, if not possible add a way to 'connect'
- [ ] Add logger
